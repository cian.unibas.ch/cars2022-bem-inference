# README

This Repository accompanies the publication [Improved distinct bone segmentation from upper-body CT using binary-prediction-enhanced multi-class inference](https://rdcu.be/cN7Lm).


## HOW TO RUN THIS CODE

### Install all packages
Create a conda environment using 
```
$ conda create --name cars22env --file ./cars22-env.txt
```
or install the packages using pip in a python 3.9 environment:
```
$ pip install -r ./requirements.txt
```
### Synthetic data creation
```
$ python3 generate_synthetic_data.py
```
### Post-processing label correction on demo data
```
$ python3 postprocessing_label_correction_demodata.py
```
### Post-processing label correction as used in the paper:
```
postprocessing_label_correction.py
```
## View the results
All files are loaded and saved in .nii nifty format. To view their content, use e.g. [3D slicer](https://www.slicer.org/).
## HOW TO CITE
If using our code, please cite the following publication:

E. Schnider, A. Huck, M. Toranelli, G. Rauter, M. Müller-Gerbl, and P. C. Cattin, “Improved distinct bone segmentation from upper-body CT using binary-prediction-enhanced multi-class inference.,” International Journal of Computer Assisted Radiology and Surgery, May 2022, doi: 10.1007/s11548-022-02650-y.

```
@article{Schnider2022May,
	author = {Schnider, Eva and Huck, Antal and Toranelli, Mireille and Rauter, Georg and M{\ifmmode\ddot{u}\else\"{u}\fi}ller-Gerbl, Magdalena and Cattin, Philippe C.},
	title = {{Improved distinct bone segmentation from upper-body CT using binary-prediction-enhanced multi-class inference.}},
	journal = {Int. J. CARS},
	pages = {1--8},
	year = {2022},
	month = may,
	issn = {1861-6429},
	publisher = {Springer International Publishing},
	doi = {10.1007/s11548-022-02650-y}
}
```
## ALGORITHM
![](label_correction_algorithm.png)
